import React, {useRef, useState, useEffect} from 'react';
import Carousel from 'react-native-snap-carousel';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {StyleSheet, View, Text, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import TabNavigator from '../../../navigation/TabNavigator';

import DetailsScreen from './detailsScreen';
import CourseNoticeScreen from './courseNoticeScreen';
import CourseScreen from './courseScreen';
import {TouchableOpacity} from 'react-native-gesture-handler';

const {width: screenWidth} = Dimensions.get('window');

const ENTRIES1 = [
  {
    icon: <Icon name="videocam-outline" size={70} />,
    text: 'Online Class',
  },
  {
    icon: <Icon name="calendar-outline" size={70} />,
    text: 'Schedule',
  },
  {
    icon: <Icon name="logo-github" size={70} />,
    text: 'Repository',
  },
  {
    icon: <Icon name="document-text-outline" size={70} />,
    text: 'Assignment',
  },
  {
    icon: <Icon name="help-outline" size={70} />,
    text: 'Doubts',
  },
  {
    icon: <Icon name="bulb-outline" size={70} />,
    text: 'Laboratory',
  },
];

const TopTab = createMaterialTopTabNavigator();

function CourseTopTabNavigator(prop) {
  return (
    <TopTab.Navigator
      screenOptions={{
        tabBarLabelStyle: {fontSize: 12, fontWeight: '700'},
        tabBarPressColor: '#4AEDE3',
        tabBarStyle: {
          backgroundColor: '#ABF1EB',
          borderBottomWidth: 0,
          borderBottomColor: '#4AEDE3',
        },
        tabBarIndicatorStyle: {
          backgroundColor: '#4AEDE3',
          borderRadius: 15,
        },
        tabBarPressOpacity: 1,
      }}>
      <TopTab.Screen name="Details" component={DetailsScreen} />
      <TopTab.Screen name="Courses" component={CourseScreen} />
      <TopTab.Screen name="Notice" component={CourseNoticeScreen} />
    </TopTab.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  carouselContainer: {
    backgroundColor: '#89E0D8',
    padding: 15,
  },
  topTabContainer: {
    flex: 1,
  },
  item: {
    width: 150,
    height: 150,
    backgroundColor: '#E9E1E1',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default CourseHeroScreen;
