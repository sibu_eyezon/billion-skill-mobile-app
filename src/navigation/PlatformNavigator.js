import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from '../screens/auth/login';
import SignupScreen from '../screens/auth/signup';
import ForgotPasswordScreen from '../screens/auth/forgotpassword';
import OTPScreen from '../screens/auth/otp';
import ResetPasswordScreen from '../screens/auth/resetPassword';
import DashboardNavigator from './DashboardNavigator';
import ReferFriend2Screen from '../screens/dashboard/referFriend2';
import CourseHeroScreen from '../screens/dashboard/courses/courseHeroScreen';
import RepositoryScreen from '../screens/dashboard/courseCarousel/courseRepository';
import AssignmentScreen from '../screens/dashboard/courseCarousel/courseAssignment';
import LabScreen from '../screens/dashboard/tools/lab';

const Stack = createStackNavigator();

function PlatformNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Signup" component={SignupScreen} />
      <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
      <Stack.Screen name="OTP" component={OTPScreen} />
      <Stack.Screen name="ResetPassword" component={ResetPasswordScreen} />
      <Stack.Screen name="Dashboard" component={DashboardNavigator} />
      <Stack.Screen name="referFriend2" component={ReferFriend2Screen} />
      <Stack.Screen name="courseHero" component={CourseHeroScreen} />
      <Stack.Screen name="repositoryScreen" component={RepositoryScreen} />
      <Stack.Screen name="assignmentScreen" component={AssignmentScreen} />
      <Stack.Screen name="labScreen" component={LabScreen} />
    </Stack.Navigator>
  );
}

export default PlatformNavigator;
