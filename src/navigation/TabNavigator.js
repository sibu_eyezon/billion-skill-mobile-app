import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import NoticeScreen from '../screens/dashboard/noticeScreen';
import Hero from '../screens/dashboard/hero';
import NotificationScreen from '../screens/dashboard/notifScreen';
import ProfileScreen from '../screens/dashboard/profileScreen';

import Icon from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

function TabNavigator() {
  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      screenOptions={{
        tabBarActiveTintColor: 'purple',
        tabBarInactiveTintColor: 'black',
        headerShown: false,
      }}>
      <Tab.Screen
        name="Notice"
        component={NoticeScreen}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="mail-outline" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Dashboard"
        component={Hero}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="apps-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Notifications"
        component={NotificationScreen}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="notifications-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="person-outline" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default TabNavigator;
