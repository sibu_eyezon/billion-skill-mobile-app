import React from 'react';

import ProfileScreen from '../screens/dashboard/profileScreen';

import ReferFriendScreen from '../screens/dashboard/referFriend';

import ApplyTeacherScreen from '../screens/dashboard/applyTeacher';
import RankActivityScreen from '../screens/dashboard/rankActivity';
import WalletScreen from '../screens/dashboard/wallet';
import CloudScreen from '../screens/dashboard/cloud';
import CareerScreen from '../screens/dashboard/career';
import AllProgramScreen from '../screens/dashboard/allProgram';

import TabNavigator from './TabNavigator';

import {createDrawerNavigator} from '@react-navigation/drawer';

import ProfileImage from '../components/profileImage';

import TestScreen from '../screens/dashboard/test';

const Drawer = createDrawerNavigator();

function MenubarNavigator() {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      name="menubar"
      screenOptions={{
        headerRight: () => <ProfileImage />,
        drawerActiveBackgroundColor: '#EFECEC',
        drawerLabelStyle: {color: 'black'},
        drawerContentStyle: {backgroundColor: '#EFECEC'},
        drawerStyle: {height: 500, width: 200},
        headerTitleStyle: {color: 'white'},
      }}>
      <Drawer.Screen name="Home" component={TabNavigator} />
      <Drawer.Screen name="All Programs" component={AllProgramScreen} />
      <Drawer.Screen name="Refer A Friend" component={ReferFriendScreen} />
      <Drawer.Screen
        name="Apply for a Teacher"
        component={ApplyTeacherScreen}
      />
      <Drawer.Screen name="My Profile" component={ProfileScreen} />
      <Drawer.Screen name="Rank & Activity" component={RankActivityScreen} />
      <Drawer.Screen name="My Wallet" component={WalletScreen} />
      <Drawer.Screen name="Digi Cloud" component={CloudScreen} />
      <Drawer.Screen name="My Career" component={CareerScreen} />
    </Drawer.Navigator>
  );
}

export default MenubarNavigator;
