import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {authReducer} from '../reducers/auth';

export default () => {
  const store = createStore(
    combineReducers({
      authReducer,
    }),
  );

  return store;
};
