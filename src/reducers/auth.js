export const authReducer = (state = {}, action) => {
  switch (action.payload) {
    case 'LOGIN':
      return {
        ...state,
        user: action.payload,
      };

    case 'SIGNUP': {
      return {
        ...state,
        user: action.payload,
      };
    }

    case 'ERROR':
      return {
        ...state,
        appError: action.payload,
      };
    default:
      return state;
  }
};
