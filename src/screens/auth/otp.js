import React, {useState} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

function OTPScreen({navigation}) {
  const [otp, setOTP] = useState({value: ''});
  return (
    <View style={styles.container}>
      <View style={styles.passwordContainer}>
        <Image
          style={styles.logo}
          source={{
            uri: 'https://files-utility.s3.ap-south-1.amazonaws.com/logomain.png',
          }}
        />
      </View>
      <View>
        <Text style={{fontWeight: '700', marginTop: 40, color: 'black'}}>
          Enter the OTP
        </Text>
        <Text
          style={{
            width: 250,
            marginTop: 20,
            fontWeight: '600',
            color: 'black',
          }}>
          Enter the OTP sent to your email and change the password.
        </Text>
        <TextInput
          style={styles.inputText}
          placeholder="Enter the OTP"
          onChangeText={otp => setOTP({value: otp})}
          placeholderTextColor="#000"
          value={otp.value}
        />
      </View>
      <TouchableOpacity
        onPress={() => {
          console.log('otp entered');
          navigation.navigate('ResetPassword');
        }}
        style={styles.loginButton}
        color="#fff">
        <Text style={{color: '#fff'}}>Submit</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#000000',
  },
  signupContainer: {
    alignItems: 'center',
    marginTop: 40,
  },
  logo: {
    justifyContent: 'center',
    width: 160,
    height: 60,
    resizeMode: 'stretch',
  },
  inputText: {
    height: 40,
    marginTop: 25,
    backgroundColor: '#EEEAEA',
    padding: 10,
    width: 250,
    color: '#000000',
  },
  loginButton: {
    backgroundColor: '#3D9CF3',
    alignItems: 'center',
    justifyContent: 'center',
    width: 250,
    padding: 10,
    marginTop: 15,
  },
});

export default OTPScreen;
