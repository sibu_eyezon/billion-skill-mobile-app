import React, {useState} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';

function ResetPasswordScreen({navigation}) {
  const [password, setPassword] = useState({value: ''});
  const [repassword, setRepassword] = useState({value: ''});
  return (
    <View style={styles.container}>
      <View style={styles.passwordContainer}>
        <Image
          style={styles.logo}
          source={{
            uri: 'https://files-utility.s3.ap-south-1.amazonaws.com/logomain.png',
          }}
        />
      </View>
      <View>
        <Text style={{fontWeight: '700', marginTop: 40, color: 'black'}}>
          Reset Password
        </Text>

        <TextInput
          style={styles.inputText}
          value={password.value}
          onChangeText={password => setPassword({value: password})}
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor="#000"
        />
        <TextInput
          style={styles.inputText}
          value={repassword.value}
          onChangeText={repassword => setRepassword({value: repassword})}
          placeholder="Retype Password"
          secureTextEntry={true}
          placeholderTextColor="#000"
        />
      </View>
      <TouchableOpacity
        onPress={() => {
          console.log('password reset button clicked');
          navigation.navigate('Login');
        }}
        style={styles.loginButton}
        color="#fff">
        <Text style={{color: '#fff'}}>Reset Password</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#000000',
  },
  signupContainer: {
    alignItems: 'center',
    marginTop: 40,
  },
  logo: {
    justifyContent: 'center',
    width: 160,
    height: 60,
    resizeMode: 'stretch',
  },
  inputText: {
    height: 40,
    marginTop: 25,
    backgroundColor: '#EEEAEA',
    padding: 10,
    width: 250,
    color: '#000000',
  },
  loginButton: {
    backgroundColor: '#3D9CF3',
    alignItems: 'center',
    justifyContent: 'center',
    width: 250,
    padding: 10,
    marginTop: 15,
  },
});

export default ResetPasswordScreen;
