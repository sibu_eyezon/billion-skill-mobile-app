import React, {useState} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';

function SignupScreen({navigation}) {
  const [fname, setFName] = useState({value: ''});
  const [lname, setLName] = useState({value: ''});
  const [email, setEmail] = useState({value: ''});
  const [password, setPassword] = useState({value: ''});
  const [repassword, setRepassword] = useState({value: ''});

  const FormData = {
    email: email.value,
    lname: fname.value,
    fname: lname.value,
    password: password.value,
    repassword: repassword.value,
  };

  const onSubmit = () => {
    if (
      FormData.email.length <= 0 ||
      FormData.fname.length <= 0 ||
      FormData.lname.length <= 0 ||
      FormData.password.length <= 0 ||
      FormData.repassword.length <= 0
    ) {
      Alert.alert(
        'Validation Error',
        'Please kindly make sure you have filled all the details properly!',
        [
          {
            text: 'Okay',

            style: 'cancel',
          },
        ],
      );
    } else if (FormData.password != FormData.repassword) {
      Alert.alert(
        'Validation Error',
        'Please kindly make sure both your passwords match!',
        [
          {
            text: 'Okay',
            style: 'cancel',
          },
        ],
      );
    } else {
      Alert.alert('Successfull', 'All data successfully taken!', [
        {
          text: 'Okay',

          style: 'cancel',
        },
      ]);
      console.log(FormData);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.signupContainer}>
        <Image
          style={styles.logo}
          source={{
            uri: 'https://files-utility.s3.ap-south-1.amazonaws.com/logomain.png',
          }}
        />
        <Text style={{color: '#0BB7A2', fontWeight: '700'}}>
          Create an Account to fill the fields below
        </Text>
      </View>
      <View style={styles.authContainer}>
        <TextInput
          style={styles.inputName}
          value={fname.value}
          onChangeText={fname => setFName({value: fname})}
          placeholder="First Name"
          placeholderTextColor="#000"
        />
        <TextInput
          style={styles.inputText}
          value={lname.value}
          onChangeText={lname => setLName({value: lname})}
          placeholder="Last Name"
          placeholderTextColor="#000"
        />
        <TextInput
          style={styles.inputText}
          placeholder="Email Address"
          onChangeText={email => setEmail({value: email})}
          placeholderTextColor="#000"
          value={email.value}
        />
        <TextInput
          style={styles.inputText}
          value={password.value}
          onChangeText={password => setPassword({value: password})}
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor="#000"
        />
        <TextInput
          style={styles.inputText}
          value={repassword.value}
          onChangeText={repassword => setRepassword({value: repassword})}
          placeholder="Retype Password"
          secureTextEntry={true}
          placeholderTextColor="#000"
        />
      </View>
      <TouchableOpacity
        onPress={onSubmit}
        style={styles.loginButton}
        color="#fff">
        <Text style={{color: '#fff'}}>Create Account Now</Text>
      </TouchableOpacity>
      <View style={styles.signinContainer}>
        <Text style={{color: 'black'}}>Already have an account?</Text>
        <Text
          style={(styles.label, {color: 'blue'})}
          onPress={() => {
            navigation.navigate('Login');
          }}>
          {'  '}Signin Now
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#000000',
  },
  signupContainer: {
    alignItems: 'center',
    marginTop: 40,
  },
  logo: {
    justifyContent: 'center',
    width: 160,
    height: 60,
    resizeMode: 'stretch',
  },
  authContainer: {
    flexDirection: 'column',
    width: 250,
    //backgroundColor: 'black',
  },
  inputName: {
    height: 40,
    marginBottom: 15,
    marginTop: 70,
    backgroundColor: '#EEEAEA',
    padding: 10,
    width: 250,
    color: '#000000',
  },
  inputText: {
    height: 40,
    marginBottom: 15,
    backgroundColor: '#EEEAEA',
    padding: 10,
    width: 250,
    color: '#000000',
  },
  loginButton: {
    backgroundColor: '#3D9CF3',
    alignItems: 'center',
    justifyContent: 'center',
    width: 250,
    padding: 10,
    marginTop: 15,
  },
  signinContainer: {
    flex: 0.15,
    flexDirection: 'row',
    marginTop: 75,
  },
});

export default SignupScreen;
