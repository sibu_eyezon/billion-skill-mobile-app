import React, {useState} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {NavigationEvents} from 'react-navigation';

function ForgotPasswordScreen({navigation}) {
  const [email, setEmail] = useState({value: ''});
  return (
    <View style={styles.container}>
      <View style={styles.passwordContainer}>
        <Image
          style={styles.logo}
          source={{
            uri: 'https://files-utility.s3.ap-south-1.amazonaws.com/logomain.png',
          }}
        />
      </View>
      <View>
        <Text style={{fontWeight: '700', marginTop: 40, color: 'black'}}>
          Forget Password?
        </Text>
        <Text
          style={{
            width: 250,
            marginTop: 20,
            fontWeight: '600',
            color: 'black',
          }}>
          Enter the email for verification and the OTP will be send to your
          email.
        </Text>
        <TextInput
          style={styles.inputText}
          placeholder="Email Address"
          onChangeText={email => setEmail({value: email})}
          placeholderTextColor="#000"
          value={email.value}
        />
      </View>
      <TouchableOpacity
        onPress={() => {
          console.log('otp pressed');
          navigation.navigate('OTP');
        }}
        style={styles.loginButton}
        color="#fff">
        <Text style={{color: '#fff'}}>Send OTP</Text>
      </TouchableOpacity>
      <Text style={{color: '#0E55C0', fontWeight: '700', marginTop: 55}}>
        Resend OTP
      </Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#000000',
  },
  signupContainer: {
    alignItems: 'center',
    marginTop: 40,
  },
  logo: {
    justifyContent: 'center',
    width: 160,
    height: 60,
    resizeMode: 'stretch',
  },
  inputText: {
    height: 40,
    marginTop: 25,
    backgroundColor: '#EEEAEA',
    padding: 10,
    width: 250,
    color: '#000000',
  },
  loginButton: {
    backgroundColor: '#3D9CF3',
    alignItems: 'center',
    justifyContent: 'center',
    width: 250,
    padding: 10,
    marginTop: 15,
  },
});

export default ForgotPasswordScreen;
