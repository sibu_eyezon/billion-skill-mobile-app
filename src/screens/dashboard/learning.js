import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {StyleSheet, View, Text} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';

const courses1 = [
  {
    id: 1,
    displayName: 'Python',
    courseStatus: 'Waiting for Approval',

    details: {
      paidDate: '9-12-2021',
      fromDate: '10-12-2021',
      toDate: '10-01-2022',
      fees: '3000',
      duration: '24',
      label: 'basic',
    },
    activity: ['10-12-2021', '12-01-2022'],
  },
  {
    id: 2,

    displayName: 'Python for Beginners',
    courseStatus: 'Enrolled',
    courseScreen: 'courseHero',
    details: {
      paidDate: '9-12-2021',
      fromDate: '10-12-2021',
      toDate: '10-01-2022',
      fees: '3000',
      duration: '24',
      label: 'basic',
    },
    activity: ['10-12-2021', '12-01-2022'],
  },
  {
    id: 3,
    displayName: 'Typescript and TailwindCSS',
    courseStatus: 'Ongoing',

    details: {
      paidDate: '9-12-2021',
      fromDate: '10-12-2021',
      toDate: '10-01-2022',
      fees: '3000',
      duration: '24',
      label: 'basic',
    },
    activity: ['10-12-2021', '12-01-2022'],
  },
  {
    id: 4,
    displayName: 'Data Science',
    courseStatus: 'Completed',

    details: {
      paidDate: '9-12-2021',
      fromDate: '10-12-2021',
      toDate: '10-01-2022',
      fees: '3000',
      duration: '24',
      label: 'basic',
    },
    activity: ['10-12-2021', '12-01-2022'],
  },
  {
    id: 5,
    displayName: 'Full Stack Developer',
    courseStatus: 'Completed',

    details: {
      paidDate: '9-12-2021',
      fromDate: '10-12-2021',
      toDate: '10-01-2022',
      fees: '3000',
      duration: '24',
      label: 'basic',
    },
    activity: ['10-12-2021', '12-01-2022'],
  },
];

function LearningScreen({navigation}) {
  const [courses, setCourses] = useState([0]);

  useEffect(() => {
    setCourses(courses1);
  }, []);

  //console.log(courses1.activity);
  return (
    <ScrollView style={styles.mainContainer}>
      {courses.length > 0 ? (
        courses.map(item =>
          item.courseStatus === 'Waiting for Approval' ? (
            <TouchableOpacity
              style={[styles.courseContainer_Main, styles.shadowContainer]}
              onPress={() => {
                navigation.navigate('courseHero', {
                  paramKey: item,
                });
              }}>
              <Icon
                name="book-outline"
                color="#360BA2"
                size={40}
                style={styles.icon}
              />
              <View key={item}>
                <Text style={{color: 'black'}}>{item.displayName}</Text>
                <Text style={{color: '#174CD3'}}>{item.courseStatus}</Text>
              </View>
            </TouchableOpacity>
          ) : item.courseStatus === 'Enrolled' ? (
            <TouchableOpacity
              style={[styles.courseContainer, styles.shadowContainer]}
              onPress={() => {
                navigation.navigate('courseHero', {
                  paramKey: item,
                });
              }}>
              <View style={{flexDirection: 'row'}}>
                <Icon
                  name="book-outline"
                  color="#360BA2"
                  size={40}
                  style={styles.icon}
                />
                <View key={item}>
                  <Text style={{color: 'black'}}>{item.displayName}</Text>
                  <Text style={{color: 'black'}}>{item.courseStatus}</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row-reverse'}}>
                <TouchableOpacity
                  style={{
                    backgroundColor: '#1583A6',
                    padding: 10,

                    borderRadius: 20,
                  }}
                  onPress={() => console.log('paid')}>
                  <Text style={{color: 'white'}}>Pay Now</Text>
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          ) : item.courseStatus === 'Completed' ? (
            <TouchableOpacity
              style={[styles.courseContainer, styles.shadowContainer]}
              onPress={() => {
                navigation.navigate('courseHero', {
                  paramKey: item,
                });
              }}>
              <View style={{flexDirection: 'row'}}>
                <Icon
                  name="book-outline"
                  color="#360BA2"
                  size={40}
                  style={styles.icon}
                />
                <View key={item}>
                  <Text style={{color: 'black'}}>{item.displayName}</Text>
                  <Text style={{color: 'red'}}>{item.courseStatus}</Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row-reverse',
                }}>
                <TouchableOpacity>
                  <Text
                    style={{
                      color: 'red',
                      fontWeight: '500',
                    }}>
                    Certificate
                  </Text>
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={[styles.courseContainer_Main, styles.shadowContainer]}
              onPress={() => {
                navigation.navigate('courseHero', {
                  paramKey: item,
                });
              }}>
              <Icon
                name="book-outline"
                color="#360BA2"
                size={40}
                style={styles.icon}
              />
              <View key={item}>
                <Text style={{color: 'black'}}>{item.displayName}</Text>
                <Text style={{color: 'green'}}>{item.courseStatus}</Text>
              </View>
            </TouchableOpacity>
          ),
        )
      ) : (
        <Text>Search for some cool courses here!</Text>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    padding: 10,
    margin: 5,
  },
  shadowContainer: {
    elevation: 20,
    shadowColor: 'black',
  },
  courseContainer: {
    flex: 1,
    padding: 10,
    margin: 10,
    backgroundColor: '#FDF5F5',
  },
  courseContainer_Main: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    margin: 10,
    backgroundColor: '#FDF5F5',
  },
  icon: {
    paddingRight: 5,
  },
});

export default LearningScreen;
