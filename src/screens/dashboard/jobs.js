import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {StyleSheet, View, Text} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';

const jobs1 = [
  {
    id: 1,
    jobName: 'Full Stack Developer',
    courseStatus: 'Interview',

    details: {
      appliedDate: '12-12-2021',
      salary: '24000 - 40000',
      place: 'Kolkata',
      interviewOn: '27-12-21',
      interviewTime: '12:00 PM',
    },
  },
  {
    id: 2,
    jobName: 'NodeJS in Kolkata',
    courseStatus: 'Rejected',

    details: {
      appliedDate: '12-12-2021',
      salary: '24000 - 40000',
      place: 'Kolkata',
      rejectedOn: '13-12-21',
    },
  },
  {
    id: 3,
    jobName: 'Looking for React and React Native',
    courseStatus: 'Interview',

    details: {
      appliedDate: '12-12-2021',
      salary: '24000 - 40000',
      place: 'Kolkata',
      interviewOn: '27-12-21',
      interviewTime: '12:00 PM',
    },
  },
  {
    id: 4,
    jobName: 'PHP MVC in CI or Laravel',
    courseStatus: 'Selected',

    details: {
      appliedDate: '12-12-2021',
      salary: '24000 - 40000',
      place: 'Kolkata',
    },
  },
  {
    id: 5,
    jobName: 'Full Stack Developer',
    courseStatus: 'Interview',

    details: {
      appliedDate: '12-12-2021',
      salary: '24000 - 40000',
      place: 'Kolkata',
      interviewOn: '27-12-21',
      interviewTime: '12:00 PM',
    },
  },
];

function JobsScreen({navigation}) {
  const [jobs, setJobs] = useState([0]);

  useEffect(() => {
    setJobs(jobs1);
  }, []);
  return (
    <ScrollView style={styles.mainContainer}>
      {jobs.length > 0 ? (
        jobs.map(item =>
          item.courseStatus === 'Interview' ? (
            // <Text>hi</Text>
            <View
              style={[styles.courseContainer_Main, styles.shadowContainer]}
              key={item.id}>
              <Icon
                name="bookmark-outline"
                color="#360BA2"
                size={40}
                style={styles.icon}
              />
              <View>
                <Text style={styles.titleLabel}>{item.jobName}</Text>
                <View style={styles.contentContainer}>
                  <Text style={styles.detailsLabel}>
                    Salary: {item.details.salary}
                  </Text>
                  <Text style={styles.detailsLabel}>{item.details.place}</Text>
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#1583A6',
                      padding: 10,
                      marginBottom: 5,
                      borderRadius: 20,
                    }}
                    onPress={() => console.log('details')}>
                    <Text style={{color: 'white'}}>Details</Text>
                  </TouchableOpacity>
                </View>

                <View style={styles.contentContainer}>
                  <Text style={[styles.detailsLabel, styles.sizeLabel]}>
                    Applied On: {item.details.appliedDate}
                  </Text>
                  <Text
                    style={{color: '#F37B0C', fontSize: 10, fontWeight: '500'}}>
                    Interview on {item.details.interviewOn}
                  </Text>
                </View>
              </View>
            </View>
          ) : item.courseStatus === 'Rejected' ? (
            // <Text>hi</Text>
            <View
              style={[styles.courseContainer_Main, styles.shadowContainer]}
              key={item.id}>
              <Icon
                name="bookmark-outline"
                color="#360BA2"
                size={40}
                style={styles.icon}
              />
              <View>
                <Text style={styles.titleLabel}>{item.jobName}</Text>
                <View style={styles.contentContainer}>
                  <Text style={styles.detailsLabel}>
                    Salary: {item.details.salary}
                  </Text>
                  <Text style={styles.detailsLabel}>{item.details.place}</Text>
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#1583A6',
                      padding: 10,
                      marginBottom: 5,
                      borderRadius: 20,
                    }}
                    onPress={() => console.log('details')}>
                    <Text style={{color: 'white'}}>Details</Text>
                  </TouchableOpacity>
                </View>

                <View style={styles.contentContainer} key={item.id}>
                  <Text style={[styles.detailsLabel, styles.sizeLabel]}>
                    Applied On: {item.details.appliedDate}
                  </Text>
                  <Text
                    style={{color: '#F30C0C', fontSize: 10, fontWeight: '500'}}>
                    Rejected on: {item.details.rejectedOn}{' '}
                  </Text>
                </View>
              </View>
            </View>
          ) : (
            <View
              style={[styles.courseContainer_Main, styles.shadowContainer]}
              key={item.id}>
              <Icon
                name="bookmark-outline"
                color="#360BA2"
                size={40}
                style={styles.icon}
              />
              <View>
                <Text style={styles.titleLabel}>{item.jobName}</Text>
                <View style={styles.contentContainer}>
                  <Text style={styles.detailsLabel}>
                    Salary: {item.details.salary}
                  </Text>
                  <Text style={styles.detailsLabel}>{item.details.place}</Text>
                  <TouchableOpacity
                    style={{
                      backgroundColor: '#1583A6',
                      padding: 10,
                      marginBottom: 5,
                      borderRadius: 20,
                    }}
                    onPress={() => console.log('details')}>
                    <Text style={{color: 'white'}}>Details</Text>
                  </TouchableOpacity>
                </View>

                <View
                  style={[styles.contentContainer, styles.selectedContainer]}
                  key={item.id}>
                  <Text style={[styles.detailsLabel, styles.sizeLabel]}>
                    Applied On: {item.details.appliedDate}
                  </Text>
                  <Text
                    style={{color: '#14BB05', fontSize: 10, fontWeight: '500'}}>
                    Selected
                  </Text>
                </View>
              </View>
            </View>
            // <Text>hi</Text>
          ),
        )
      ) : (
        <Text>Search for some cool courses here!</Text>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    padding: 10,
    margin: 5,
  },

  contentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
  },
  selectedContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '75%',
  },
  titleLabel: {
    color: 'black',
    fontWeight: '500',
    fontSize: 15,
  },
  detailsLabel: {
    color: '#174CD3',
  },
  sizeLabel: {
    fontSize: 10,
    fontWeight: '500',
  },
  shadowContainer: {
    elevation: 20,
    shadowColor: 'black',
  },
  courseContainer: {
    flex: 1,
    padding: 10,
    margin: 10,
    backgroundColor: '#FDF5F5',
  },
  courseContainer_Main: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    margin: 10,
    backgroundColor: '#FDF5F5',
  },
  icon: {
    paddingRight: 5,
  },
});

export default JobsScreen;
