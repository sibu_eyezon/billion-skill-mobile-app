import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Select, NativeBaseProvider, Center} from 'native-base';

export default function ReferFriend({navigation}) {
  const [program, setProgram] = useState({value: ''});
  const [name, setName] = useState({value: ''});
  const [email, setEmail] = useState({value: ''});
  const [mobile, setMobile] = useState({value: ''});

  console.log(program);
  const data = {
    program: program,
    name: name.value,
    email: email.value,
    mobile: mobile.value,
  };

  return (
    <NativeBaseProvider style={styles.container}>
      <Center flex={1}>
        <Select
          label="Program List"
          style={[styles.dropdownContainer]}
          selectedValue={program}
          minWidth={200}
          placeholderTextColor="black"
          placeholder="Program List"
          onValueChange={itemValue => setProgram(itemValue)}>
          <Select.Item label="JavaScript" value="JavaScript" />
          <Select.Item label="TypeScript" value="TypeScript" />
          <Select.Item label="C" value="C" />
          <Select.Item label="Python" value="Python" />
          <Select.Item label="Java" value="Java" />
        </Select>
        <TextInput
          style={styles.inputBox}
          value={email.value}
          onChangeText={email => setEmail({value: email})}
          placeholder="Enter Email ID"
          placeholderTextColor="#000"
        />
        <TextInput
          style={styles.inputBox}
          value={name.value}
          onChangeText={name => setName({value: name})}
          placeholder="Full Name"
          placeholderTextColor="#000"
        />

        <TextInput
          style={styles.inputBox}
          value={mobile.value}
          keyboardType="numeric"
          onChangeText={mobile => setMobile({value: mobile})}
          placeholder="Mobile Number"
          placeholderTextColor="#000"
        />
      </Center>
      <View style={{alignItems: 'center', marginBottom: 50}}>
        <TouchableOpacity
          style={styles.submitButton}
          //onPress={referPage2(data)}
        >
          <Text
            style={{color: 'white'}}
            onPress={() => {
              navigation.navigate('referFriend2', {
                paramKey: data,
              });
              console.log(data);
            }}>
            Submit Now
          </Text>
        </TouchableOpacity>
      </View>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  dropdownContainer: {
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#D3DAD9',

    width: 350,

    fontSize: 15,
    fontWeight: '500',
  },
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
  },
  inputBox: {
    backgroundColor: '#D3DAD9',
    fontWeight: '500',
    margin: 25,
    fontSize: 15,
    width: 350,
  },
  submitButton: {
    backgroundColor: '#1583A6',
    padding: 10,
    borderRadius: 10,
    width: 100,
  },
});
