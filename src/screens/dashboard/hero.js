import React, {useRef, useState, useEffect} from 'react';
import Carousel from 'react-native-snap-carousel';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import LearningScreen from '../dashboard/learning';
import JobsScreen from '../dashboard/jobs';
import EventsScreen from '../dashboard/events';

const {width: screenWidth} = Dimensions.get('window');

const ENTRIES1 = [
  {
    icon: <Icon name="videocam-outline" size={70} color="black" />,
    text: 'Online Class',
    courseScreen: 'classScreen',
  },
  {
    icon: <Icon name="stats-chart-outline" size={70} color="black" />,
    text: 'Feedback Loop',
    courseScreen: 'classScreen',
  },
  {
    icon: <Icon name="alarm-outline" size={70} color="black" />,
    text: 'Schedule',
    courseScreen: 'scheduleScreen',
  },
  {
    icon: <Icon name="bulb-outline" size={70} color="black" />,
    text: 'Laboratory',
    courseScreen: 'labScreen',
  },
  {
    icon: <Icon name="document-outline" size={70} color="black" />,
    text: 'Resume',
    courseScreen: 'resumeScreen',
  },
  {
    icon: <Icon name="wallet-outline" size={70} color="black" />,
    text: 'Upload Certificate',
    courseScreen: 'uploadCertifScreen',
  },
  {
    icon: <Icon name="cloud-upload-outline" size={70} color="black" />,
    text: 'walletScreen',
  },
];

const TopTab = createMaterialTopTabNavigator();

function Hero({navigation}) {
  const [entries, setEntries] = useState([]);
  const carouselRef = useRef(null);

  useEffect(() => {
    setEntries(ENTRIES1);
  }, []);

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() => {
          navigation.navigate(`${item.courseScreen}`);
        }}>
        <Text style={styles.image}>{item.icon}</Text>
        <Text style={{color: 'black'}}>{item.text}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.carouselContainer}>
        <Carousel
          ref={carouselRef}
          sliderWidth={screenWidth}
          sliderHeight={screenWidth}
          itemWidth={150}
          itemHeight={150}
          data={entries}
          renderItem={renderItem}
        />
      </View>
      <View style={styles.topTabContainer}>
        <TopTab.Navigator
          screenOptions={{
            tabBarLabelStyle: {fontSize: 12, fontWeight: '700'},
            tabBarPressColor: '#4AEDE3',
            tabBarStyle: {
              backgroundColor: '#ABF1EB',
              borderBottomWidth: 0,
              borderBottomColor: '#4AEDE3',
            },
            tabBarIndicatorStyle: {
              backgroundColor: '#4AEDE3',
              borderRadius: 15,
            },
            tabBarPressOpacity: 1,
          }}>
          <TopTab.Screen name="My Learnings" component={LearningScreen} />
          <TopTab.Screen name="Jobs" component={JobsScreen} />
          <TopTab.Screen name="Events" component={EventsScreen} />
        </TopTab.Navigator>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  carouselContainer: {
    backgroundColor: '#89E0D8',
    padding: 15,
    color: 'black',
  },
  topTabContainer: {
    flex: 1,
  },
  item: {
    width: 150,
    height: 150,
    backgroundColor: '#E9E1E1',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Hero;
