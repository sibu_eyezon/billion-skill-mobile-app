import React from 'react';

import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

function DetailsScreen({navigation, route}) {
  return (
    <View style={styles.container}>
      <View style={styles.detailsContainer}>
        <Text style={styles.title}>Details</Text>
        <Text style={styles.content}>
          Present Status: {route.params.paramKey.courseStatus}
        </Text>
        <Text style={styles.content}>
          Payment Status: Paid on {route.params.paramKey.details.paidDate}
        </Text>
        <Text style={styles.content}>
          Date:{route.params.paramKey.details.fromDate} to{' '}
          {route.params.paramKey.details.toDate}
        </Text>
        <Text style={styles.content}>
          Fees: {route.params.paramKey.details.fees}
        </Text>
        <Text style={styles.content}>
          Duration: {route.params.paramKey.details.duration} Hours
        </Text>
        <Text style={styles.content}>
          Label: {route.params.paramKey.details.label}
        </Text>
      </View>

      <View style={styles.detailsContainer}>
        <Text style={styles.title}>Activties</Text>

        <Text style={styles.content}>
          {route.params.paramKey.activity.length > 0 ? (
            // <Text>Working</Text>
            route.params.paramKey.activity.map(element => (
              <Text>
                ✔️ You have submitted the assignment on {element}.{'\n'}
              </Text>
            ))
          ) : (
            <Text>No Assignments have been submitted yet.</Text>
          )}
        </Text>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={[styles.iconButton, styles.feedbackButton]}>
          <Text style={{color: 'white'}}>Give Feedback</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton}>
          <Icon name="share-social-outline" size={40} color="#2E3A59" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton}>
          <Icon name="people-outline" size={40} color="#2E3A59" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  detailsContainer: {
    margin: 25,
    padding: 5,
    borderColor: '#89E0D8',
    borderWidth: 2,
    borderRadius: 5,
  },
  title: {
    fontSize: 20,
    color: 'black',
    fontWeight: '700',
    marginBottom: 10,
  },
  content: {
    fontWeight: '500',
    color: 'black',
  },
  buttonContainer: {
    flexDirection: 'row-reverse',
  },
  iconButton: {
    margin: 5,
    padding: 5,
    alignContent: 'center',
    justifyContent: 'center',
    width: 50,
    borderRadius: 10,
    color: '#2E3A59',
    backgroundColor: '#EF9D21',
  },
  feedbackButton: {
    width: 105,
    fontWeight: '500',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#1583A6',
  },
});

export default DetailsScreen;
