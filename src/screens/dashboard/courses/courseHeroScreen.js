import React, {useRef, useState, useEffect} from 'react';
import Carousel from 'react-native-snap-carousel';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import TabNavigator from '../../../navigation/TabNavigator';
import NavbarCourse from '../../../components/navbarCourse';

import DetailsScreen from './detailsScreen';
import CourseNoticeScreen from './courseNoticeScreen';
import CourseScreen from './courseScreen';

const {width: screenWidth} = Dimensions.get('window');

const ENTRIES1 = [
  {
    icon: <Icon name="videocam-outline" size={70} color="black" />,
    displayName: 'Online Class',
    courseScreen: 'onlineClassScreen',
  },
  {
    icon: <Icon name="calendar-outline" size={70} color="black" />,
    displayName: 'Schedule',
    courseScreen: 'scheduleScreen',
  },
  {
    icon: <Icon name="logo-github" size={70} color="black" />,
    displayName: 'Repository',
    courseScreen: 'repositoryScreen',
  },
  {
    icon: <Icon name="document-text-outline" size={70} color="black" />,
    displayName: 'Assignment',
    courseScreen: 'assignmentScreen',
  },
  {
    icon: <Icon name="help-outline" size={70} color="black" />,
    displayName: 'Doubts',
    courseScreen: 'doubtsScreen',
  },
  {
    icon: <Icon name="bulb-outline" size={70} color="black" />,
    displayName: 'Laboratory',
    courseScreen: 'labScreen',
  },
];

const TopTab = createMaterialTopTabNavigator();

function CourseHeroScreen({navigation, route}) {
  const [entries, setEntries] = useState([]);
  const carouselRef = useRef(null);
  const data = route.params.paramKey;

  //console.log(data);

  useEffect(() => {
    setEntries(ENTRIES1);
  }, []);

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() => {
          navigation.navigate(`${item.courseScreen}`, {
            paramKey: item,
          });
        }}>
        <Text style={styles.image}>{item.icon}</Text>
        <Text style={{color: 'black'}}>{item.displayName}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <NavbarCourse props={data} />
      <View style={styles.carouselContainer}>
        <Carousel
          ref={carouselRef}
          sliderWidth={screenWidth}
          sliderHeight={screenWidth}
          itemWidth={150}
          itemHeight={150}
          data={entries}
          renderItem={renderItem}
        />
      </View>
      <View style={styles.topTabContainer}>
        <TopTab.Navigator
          screenOptions={{
            tabBarLabelStyle: {fontSize: 12, fontWeight: '700'},
            tabBarPressColor: '#4AEDE3',
            tabBarStyle: {
              backgroundColor: '#ABF1EB',
              borderBottomWidth: 0,
              borderBottomColor: '#4AEDE3',
            },
            tabBarIndicatorStyle: {
              backgroundColor: '#4AEDE3',
              borderRadius: 15,
            },
            tabBarPressOpacity: 1,
          }}>
          <TopTab.Screen
            name="Details"
            component={DetailsScreen}
            initialParams={{paramKey: data}}
          />
          <TopTab.Screen name="Courses" component={CourseScreen} />
          <TopTab.Screen name="Notice" component={CourseNoticeScreen} />
        </TopTab.Navigator>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  carouselContainer: {
    backgroundColor: '#89E0D8',
    padding: 15,
  },
  topTabContainer: {
    flex: 1,
  },
  item: {
    width: 150,
    height: 150,
    backgroundColor: '#E9E1E1',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default CourseHeroScreen;
