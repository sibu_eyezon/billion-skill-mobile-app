import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {StyleSheet, View, Text} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';

const labs1 = [
  {
    id: 1,
    labName: 'Python Laboratory',
    sharedOn: '12-12-2021',
  },
  {
    id: 2,
    labName: 'Java Laboratory',
    sharedOn: '12-12-2021',
  },
  {
    id: 3,
    labName: 'PHP Laboratory',
    sharedOn: '12-12-2021',
  },
  {
    id: 4,
    labName: 'C++ Laboratory',
    sharedOn: '12-12-2021',
  },
  {
    id: 5,
    labName: 'Python Laboratory',
    sharedOn: '12-12-2021',
  },
  {
    id: 6,
    labName: 'Python Laboratory',
    sharedOn: '12-12-2021',
  },
  {
    id: 7,
    labName: 'Python Laboratory',
    sharedOn: '12-12-2021',
  },
  {
    id: 8,
    labName: 'Python Laboratory',
    sharedOn: '12-12-2021',
  },
  {
    id: 9,
    labName: 'Python Laboratory',
    sharedOn: '12-12-2021',
  },
];

function FaciltiesScreen({navigation}) {
  const [labs, setLab] = useState('');

  useEffect(() => {
    setLab(labs1);
  }, []);
  return (
    <ScrollView style={styles.mainContainer}>
      {labs.length > 0 ? (
        labs.map(item => (
          // <Text>hi</Text>
          <View
            style={[styles.courseContainer_Main, styles.shadowContainer]}
            key={item.id}>
            <Icon
              name="code-slash-outline"
              color="#360BA2"
              size={40}
              style={styles.icon}
            />
            <View style={styles.contentContainer}>
              <Text style={styles.titleLabel}>{item.labName}</Text>

              <Text style={styles.detailsLabel}>
                Shared on: {item.sharedOn}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                backgroundColor: '#1583A6',
                padding: 10,
                marginBottom: 5,
                borderRadius: 20,
              }}
              onPress={() => console.log('details')}>
              <Text style={{color: 'white'}}>Details</Text>
            </TouchableOpacity>
          </View>
        ))
      ) : (
        <Text>No Labs Available at the Moment!</Text>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    padding: 10,
    margin: 5,
  },

  contentContainer: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '60%',
  },
  titleLabel: {
    color: 'black',
    fontWeight: '500',
    fontSize: 15,
  },
  detailsLabel: {
    color: '#174CD3',
  },
  sizeLabel: {
    fontSize: 10,
    fontWeight: '500',
  },
  shadowContainer: {
    elevation: 20,
    shadowColor: 'black',
  },
  courseContainer: {
    flex: 1,
    padding: 10,
    margin: 10,
    backgroundColor: '#FDF5F5',
  },
  courseContainer_Main: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    margin: 10,
    backgroundColor: '#FDF5F5',
  },
  icon: {
    paddingRight: 5,
  },
});

export default FaciltiesScreen;
