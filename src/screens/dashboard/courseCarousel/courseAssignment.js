import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Linking,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import NavbarCourse from '../../../components/navbarCourse';

const assignment1 = [
  {
    id: 1,
    submitted: true,
    checked: false,
    details: {
      title: 'Assignment 12',
      addedOn: '24-12-2021',
      deadline: '29-12-2021',
      deadlineTime: '12 PM',
      downloadLink: 'http://skaushik.xyz/',
      submittedOn: '28-12-2021',
    },
  },

  {
    id: 2,
    submitted: false,
    checked: false,
    details: {
      title: 'Assignment 11',
      addedOn: '23-12-2021',
      deadline: '29-12-2021',
      deadlineTime: '12 PM',
      downloadLink: 'http://skaushik.xyz/',
    },
  },
  {
    id: 3,
    submitted: true,
    checked: true,
    details: {
      title: 'Assignment 10',
      addedOn: '28-12-2021',
      deadline: '29-12-2021',
      time: '12 PM',
      downloadLink: 'http://skaushik.xyz/',
      checkedOn: '29-12-2001',
      remarks: 'Excellent Work!',
    },
  },
  {
    id: 3,
    submitted: true,
    checked: false,
    details: {
      title: 'Assignment 11',
      addedOn: '28-12-2021',
      deadline: '29-12-2021',
      time: '12 PM',
      submittedOn: '28-12-2021',
    },
  },
];

function AssignmentScreen({route}) {
  const [assignment, setAssignment] = useState('');

  useEffect(() => {
    setAssignment(assignment1);
  }, []);
  //console.log(courses);
  return (
    <View>
      <NavbarCourse props={route.params.paramKey} />
      <ScrollView style={styles.mainContainer}>
        {assignment.length > 0 ? (
          assignment.map(item =>
            item.submitted == false ? (
              // <Text style={styles.titleLabel}>{item.details.title}</Text>
              <View style={styles.assignContainer}>
                <Icon name="document-outline" color="#360BA2" size={40} />
                <View style={styles.textContainer}>
                  <Text style={styles.titleLabel}>{item.details.title}</Text>

                  <View style={styles.contentContainer}>
                    <Text style={styles.detailsLabel}>
                      Added on: {item.details.addedOn}
                    </Text>
                    <Text style={styles.detailsLabel}>
                      Deadline: {item.details.deadline}
                    </Text>
                    <Text style={styles.detailsLabel}>
                      {item.details.deadlineTime}
                    </Text>
                  </View>
                  <View style={styles.contentContainer}>
                    <TouchableOpacity
                      style={styles.downloadButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.downloadLink}`);
                      }}>
                      <Icon name="download-outline" color="black" size={25} />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.submitButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.downloadLink}`);
                      }}>
                      <Text style={styles.label}>Submit Now</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : item.submitted == true && item.checked == false ? (
              <View style={styles.assignContainer}>
                <Icon name="document-outline" color="#360BA2" size={40} />
                <View style={styles.textContainer}>
                  <Text style={styles.titleLabel}>{item.details.title}</Text>

                  <View style={styles.contentContainer}>
                    <Text style={styles.detailsLabel}>
                      Added on: {item.details.addedOn}
                    </Text>
                    <Text style={styles.detailsLabel}>
                      Deadline: {item.details.deadline}
                    </Text>
                    <Text style={styles.detailsLabel}>
                      {item.details.deadlineTime}
                    </Text>
                  </View>
                  <View style={styles.contentContainer}>
                    <TouchableOpacity
                      style={styles.downloadButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.downloadLink}`);
                      }}>
                      <Icon name="download-outline" color="black" size={25} />
                    </TouchableOpacity>
                    <Text style={styles.submitLabel}>
                      Submitted On: {item.details.submittedOn}
                    </Text>
                  </View>
                </View>
              </View>
            ) : (
              <View style={styles.assignContainer}>
                <Icon name="document-outline" color="#360BA2" size={40} />
                <View style={styles.textContainer}>
                  <Text style={styles.titleLabel}>{item.details.title}</Text>

                  <View style={styles.contentContainer}>
                    <Text style={styles.detailsLabel}>
                      Added on: {item.details.addedOn}
                    </Text>
                    <Text style={styles.detailsLabel}>
                      Deadline: {item.details.deadline}
                    </Text>
                    <Text style={styles.detailsLabel}>
                      {item.details.deadlineTime}
                    </Text>
                  </View>
                  <View style={styles.contentContainer}>
                    <TouchableOpacity
                      style={styles.downloadButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.downloadLink}`);
                      }}>
                      <Icon name="download-outline" color="black" size={25} />
                    </TouchableOpacity>

                    <Text style={[styles.submitLabel, styles.checkedLabel]}>
                      Checked On: {item.details.checkedOn}
                    </Text>
                    <TouchableOpacity
                      style={styles.downloadButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.downloadLink}`);
                      }}>
                      <Text style={styles.label}>View Remarks</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ),
          )
        ) : (
          <Text>No Assignments yet!</Text>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    padding: 10,
    margin: 5,
  },
  assignContainer: {
    padding: 10,
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#89E0D8',
    margin: 5,
  },
  textContainer: {
    flexDirection: 'column',
    alignContent: 'center',
    width: '85%',
  },
  contentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleLabel: {
    color: 'black',
    fontWeight: '500',
    fontSize: 15,
  },
  detailsLabel: {
    color: 'black',
    fontSize: 12,
  },
  submitLabel: {
    color: 'red',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 5,
  },
  checkedLabel: {
    color: '#BC6907',
  },
  downloadButton: {
    padding: 5,
    margin: 5,
    borderRadius: 7,
    backgroundColor: '#CAC8C5',
  },
  submitButton: {
    padding: 5,
    margin: 5,
    borderRadius: 7,
    backgroundColor: '#E9AE17',
  },
  label: {
    color: 'black',
    fontWeight: '500',
  },
});

export default AssignmentScreen;
