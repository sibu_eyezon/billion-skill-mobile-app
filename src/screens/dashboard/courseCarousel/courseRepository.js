import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Linking,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import NavbarCourse from '../../../components/navbarCourse';

const repository1 = [
  {
    id: 1,
    type: 'Question Paper',
    details: {
      title: 'Sample Question Paper',
      addedOn: '24-12-2021',
      visibility: 'Private',
      downloadLink: 'https://aboutreact.com',
      readOpenLink: 'https://aboutreact.com',
    },
  },

  {
    id: 2,
    type: 'Video Lecture',
    details: {
      title: 'Lecture 5',
      addedOn: '23-12-2021',
      visibility: 'Public',
      downloadLink: 'https://aboutreact.com',
      youtubeLink: 'hhttps://www.youtube.com/',
    },
  },
  {
    id: 3,
    type: 'Video Lecture',
    details: {
      title: 'Lecture 4',
      addedOn: '28-12-2021',
      visibility: 'Public',
      downloadLink: 'https://aboutreact.com',
      youtubeLink: 'https://www.youtube.com/',
    },
  },
  {
    id: 4,
    type: 'Video Lecture',
    details: {
      title: 'Lecture 4',
      addedOn: '28-12-2021',
      visibility: 'Public',
      downloadLink: 'https://aboutreact.com',
      youtubeLink: 'https://www.youtube.com/',
    },
  },
  {
    id: 5,
    type: 'Video Lecture',
    details: {
      title: 'Lecture 4',
      addedOn: '28-12-2021',
      visibility: 'Public',
      downloadLink: 'https://aboutreact.com',
      youtubeLink: 'https://www.youtube.com/',
    },
  },
  {
    id: 6,
    type: 'Video Lecture',
    details: {
      title: 'Lecture 4',
      addedOn: '28-12-2021',
      visibility: 'Public',
      downloadLink: 'https://aboutreact.com',
      youtubeLink: 'https://www.youtube.com/',
    },
  },
  {
    id: 7,
    type: 'Video Lecture',
    details: {
      title: 'Lecture 4',
      addedOn: '28-12-2021',
      visibility: 'Public',
      downloadLink: 'https://aboutreact.com',
      youtubeLink: 'https://www.youtube.com/',
    },
  },
  {
    id: 8,
    type: 'Video Lecture',
    details: {
      title: 'Lecture 4',
      addedOn: '28-12-2021',
      visibility: 'Public',
      downloadLink: 'https://aboutreact.com',
      youtubeLink: 'https://www.youtube.com/',
    },
  },
  {
    id: 9,
    type: 'Video Lecture',
    details: {
      title: 'Lecture 4',
      addedOn: '28-12-2021',
      visibility: 'Public',
      downloadLink: 'https://aboutreact.com',
      youtubeLink: 'https://www.youtube.com/',
    },
  },
];

function RepositoryScreen({route}) {
  const [repository, setRepository] = useState([0]);

  useEffect(() => {
    setRepository(repository1);
  }, []);
  console.log(route);
  return (
    <View>
      <NavbarCourse props={route.params.paramKey} />
      <ScrollView style={styles.mainContainer}>
        {repository.length > 0 ? (
          repository.map(item =>
            item.type === 'Question Paper' ? (
              <View style={styles.repoContainer}>
                <Icon name="document-outline" color="#360BA2" size={40} />
                <View style={styles.textContainer}>
                  <Text style={styles.titleLabel}>{item.details.title}</Text>

                  <View style={styles.contentContainer}>
                    <Text style={styles.detailsLabel}>
                      Added on: {item.details.addedOn}
                    </Text>
                    <Text style={styles.detailsLabel}>
                      {item.details.visibility}
                    </Text>
                  </View>
                  <View style={styles.contentContainer}>
                    <TouchableOpacity
                      style={styles.downloadButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.downloadLink}`);
                      }}>
                      <Text style={styles.label}>Download Now</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.readButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.downloadLink}`);
                      }}>
                      <Text style={styles.label}>Read Now</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ) : (
              <View style={styles.repoContainer}>
                <Icon name="document-outline" color="#360BA2" size={40} />
                <View style={styles.textContainer}>
                  <Text style={styles.titleLabel}>{item.details.title}</Text>

                  <View style={styles.contentContainer}>
                    <Text style={styles.detailsLabel}>
                      Added on: {item.details.addedOn}
                    </Text>
                    <Text style={styles.detailsLabel}>
                      {item.details.visibility}
                    </Text>
                  </View>
                  <View style={styles.contentContainer}>
                    <TouchableOpacity
                      style={styles.readButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.downloadLink}`);
                      }}>
                      <Text style={styles.label}>Download Now</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.videoButton}
                      onPress={() => {
                        Linking.openURL(`${item.details.youtubeLink}`);
                      }}>
                      <Text style={styles.label}>View in Youtube</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            ),
          )
        ) : (
          <Text>Nothing in your respository right now!</Text>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    padding: 10,
    margin: 5,
  },
  repoContainer: {
    padding: 10,
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#89E0D8',
    margin: 5,
  },
  textContainer: {
    flexDirection: 'column',
    alignContent: 'center',
    width: '90%',
  },
  contentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleLabel: {
    color: 'black',
    fontWeight: '500',
    fontSize: 15,
  },
  detailsLabel: {
    color: 'black',
  },
  downloadButton: {
    padding: 5,
    margin: 5,
    borderRadius: 7,
    backgroundColor: '#15A660',
  },
  readButton: {
    padding: 5,
    margin: 5,
    borderRadius: 7,
    backgroundColor: '#E9AE17',
  },
  videoButton: {
    padding: 5,
    margin: 5,
    borderRadius: 7,
    backgroundColor: '#E96317',
  },
  label: {
    color: 'white',
  },
});

export default RepositoryScreen;
