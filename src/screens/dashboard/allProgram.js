import React, {useState} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {Select, NativeBaseProvider, Center} from 'native-base';
import AllProgram from '../../components/program';

function AllProgramScreen({navigation}) {
  const [search, setSearch] = useState('');
  const [program, setProgram] = useState('');

  const updateSearch = search => {
    setSearch(search);
    console.log(search);
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <View style={styles.view}>
          <SearchBar
            containerStyle={{
              backgroundColor: '#80CBC4',
              borderTopColor: '#80CBC4',
              borderBottomColor: '#80CBC4',
            }}
            inputContainerStyle={{backgroundColor: '#EEEAEA'}}
            inputStyle={{color: 'black'}}
            placeholder="Type Here..."
            onChangeText={updateSearch}
            value={search}
          />
        </View>
      </View>
      <ScrollView>
        <NativeBaseProvider>
          <Center flex={1}>
            <Select
              label="Filter"
              style={[styles.dropdownContainer]}
              selectedValue={program}
              minWidth={120}
              placeholderTextColor="black"
              placeholder="Filter"
              onValueChange={itemValue => setProgram(itemValue)}>
              <Select.Item label="Alphabetical" value="alphabetical" />
              <Select.Item label="Price Low to High" value="price" />
              <Select.Item label="Newest Course" value="latest" />
              <Select.Item label="Most Popular" value="popular" />
            </Select>
          </Center>
        </NativeBaseProvider>

        <AllProgram />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  view: {
    margin: 10,
  },
  dropdownContainer: {
    alignContent: 'center',
    justifyContent: 'center',

    width: 150,
    fontSize: 15,
    fontWeight: '500',
  },
  searchContainer: {
    backgroundColor: '#80CBC4',
    padding: 5,
  },
  label: {
    fontSize: 15,
    fontWeight: '500',
    margin: 5,
    color: 'black',
    flexDirection: 'row-reverse',
  },

  filterInput: {
    width: '75%',
    backgroundColor: '#EEEAEA',
    borderRadius: 15,
  },
  searchButton: {
    backgroundColor: '#C4F2EE',
    borderRadius: 50,
    width: 50,
  },
});

export default AllProgramScreen;
