import React from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';

function ReferFriend2Screen({route, navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text style={styles.text}>
          Dear Member, {'\n\n'}You have successfully referred your friend{' '}
          {route.params.paramKey.name} for the program of{' '}
          {route.params.paramKey.program}. {'\n\n\n'}Once your friend accepts
          the program and get admitted, you will receive the following:{' '}
          {'\n\n\n'} - 10% of the Program Fees.
        </Text>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.walletButton}>
          <Text style={{color: 'white'}}>My Wallet</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.dashboardButton}
          onPress={() => {
            navigation.navigate('Home');
          }}>
          <Text style={{color: 'white'}}>Back to Dashboard</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    margin: 30,
  },
  textContainer: {
    textAlign: 'justify',
  },
  text: {
    color: 'black',
    fontSize: 15,
    fontWeight: '500',
  },
  buttonContainer: {
    marginTop: 150,
    flexDirection: 'row',
  },
  walletButton: {
    width: 100,
    borderRadius: 15,
    marginRight: 80,
    backgroundColor: '#F09E23',
    padding: 15,
  },
  dashboardButton: {
    width: 150,
    borderRadius: 15,
    backgroundColor: '#1583A6',
    padding: 15,
  },
});

export default ReferFriend2Screen;
