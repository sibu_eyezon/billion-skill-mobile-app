import React from 'react';

import {StyleSheet, View, Image} from 'react-native';

const ProfileImage = ({navigation}) => {
  return (
    <View style={styles.imageContainer}>
      <Image
        style={styles.profileImage}
        source={{
          uri: 'https://images.unsplash.com/photo-1638602840925-7b205d196df5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80',
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    flex: 0.7,
    alignContent: 'center',
    justifyContent: 'space-between',
  },
  profileImage: {
    width: 35,
    height: 35,
    borderRadius: 100,
    marginRight: 5,
  },
});

export default ProfileImage;
