import React from 'react';

import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';

const NavbarCourse = props => {
  const navigation = useNavigation();
  return (
    <View style={styles.navContainer}>
      <Text style={styles.content}>{props.props.displayName}</Text>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Icon name="home-outline" size={30} color="#2E3A59" />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  navContainer: {
    backgroundColor: '#ABF1EB',
    height: 50,
    padding: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  content: {
    color: 'black',
    fontSize: 25,
  },
});

export default NavbarCourse;
