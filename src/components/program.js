import React, {useRef, useState, useEffect} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';

const programs = [
  {
    courseName: 'Python for Begineers',
    pay: '3000',
    courseDuration: '20',
    label: 'basic',
    deadline: '12-12-21',
    courseStatus: 'Applied',
  },
  {
    courseName: 'Typescript',
    pay: '3000',
    courseDuration: '20',
    label: 'basic',
    deadline: '12-12-21',
    courseStatus: 'Apply Now',
  },
  {
    courseName: 'Google Cloud',
    pay: '3000',
    courseDuration: '20',
    label: 'basic',
    deadline: '12-12-21',
    courseStatus: 'Applied',
  },
  {
    courseName: 'Google Cloud',
    pay: '5000',
    courseDuration: '40',
    label: 'Advanced',
    deadline: '12-12-21',
    courseStatus: 'Applied',
  },
];

function AllProgram({navigation}) {
  const [program, setProgram] = useState([]);

  useEffect(() => {
    setProgram(programs);
  }, []);

  return (
    <ScrollView>
      {program.length > 0 ? (
        program.map(item =>
          item.courseStatus === 'Applied' ? (
            <View style={styles.container}>
              <View style={[styles.courseContainer, styles.shadowContainer]}>
                <Image
                  style={styles.courseImage}
                  source={require('../assets/courseImage.png')}
                />
                <View style={styles.textContainer}>
                  <Text style={[styles.courseLabel, styles.courseTitle]}>
                    {item.courseName}
                  </Text>
                  <View style={styles.payContainer}>
                    <Text style={[styles.courseLabel, styles.payLabel]}>
                      Rs {item.pay}
                    </Text>
                    <Text style={styles.courseLabel}>
                      {item.courseDuration} Hours
                    </Text>
                  </View>
                  <View style={styles.payContainer}>
                    <Text style={[styles.courseLabel, styles.payLabel]}>
                      {item.label}
                    </Text>
                    <Text style={styles.courseLabel}>
                      Deadline: {item.deadline}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <TouchableOpacity>
                      <Text style={{color: '#3D9CF3', fontWeight: '500'}}>
                        Details
                      </Text>
                    </TouchableOpacity>
                    <Text style={{color: 'black', fontWeight: '700'}}>
                      {item.courseStatus}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={styles.container}>
              <View style={[styles.courseContainer, styles.shadowContainer]}>
                <Image
                  style={styles.courseImage}
                  source={require('../assets/courseImage.png')}
                />
                <View style={styles.textContainer}>
                  <Text style={[styles.courseLabel, styles.courseTitle]}>
                    {item.courseName}
                  </Text>
                  <View style={styles.payContainer}>
                    <Text style={[styles.courseLabel, styles.payLabel]}>
                      Rs {item.pay}
                    </Text>
                    <Text style={styles.courseLabel}>
                      {item.courseDuration} Hours
                    </Text>
                  </View>
                  <View style={styles.payContainer}>
                    <Text style={[styles.courseLabel, styles.payLabel]}>
                      {item.label}
                    </Text>
                    <Text style={styles.courseLabel}>
                      Deadline: {item.deadline}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <TouchableOpacity>
                      <Text style={{color: '#3D9CF3', fontWeight: '500'}}>
                        Details
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <Text style={{color: '#3D9CF3', fontWeight: '500'}}>
                        {item.courseStatus}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          ),
        )
      ) : (
        <Text>No Courses Available at the moment!</Text>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 15,
  },
  courseContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
  },
  shadowContainer: {
    elevation: 20,
    shadowColor: 'black',
  },
  textContainer: {
    flexDirection: 'column',
    alignContent: 'center',
    width: '65%',
  },
  payContainer: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
  },
  courseImage: {
    width: 100,
    height: 100,
    marginRight: 10,
    backgroundColor: '#C4C4C4',
    borderRadius: 100,
    padding: 5,
  },
  courseLabel: {
    fontSize: 15,
    color: 'black',
  },

  courseTitle: {
    fontWeight: '500',
    marginTop: 15,
  },
});

export default AllProgram;
