import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';

import LoginScreen from './src/screens/auth/login';
import PlatformNavigator from './src/navigation/PlatformNavigator';

import configureStore from './src/store/configureStore';
import SignupScreen from './src/screens/auth/signup';

const store = configureStore();

export default function App() {
  return (
    // <View style={styles.container}>
    //   <Text>Open up App.js to startgfg working sson your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
    <NavigationContainer>
      <Provider store={store}>
        <PlatformNavigator />
      </Provider>
    </NavigationContainer>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
